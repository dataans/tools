use lazy_static::lazy_static;
use regex::Regex;
use thiserror::Error;

use crate::{Error as GeneralError, ValidationResult};

#[derive(Error, Debug)]
pub enum UserValidationError {
    #[error("Invalid username")]
    InvalidUsername,
    #[error("Invalid password: {0}")]
    InvalidPassword(String),
    #[error("Invalid full name")]
    InvalidFullName,
    #[error("Invalid email")]
    InvalidEmail,
}

lazy_static! {
    static ref USERNAME_REGEX: Regex = Regex::new(r"^[a-zA-Z0-9]+([._]?[a-zA-Z0-9]+)*$").unwrap();
    static ref EMAIL_REGEX: Regex = Regex::new(r"^[a-zA-Z0-9.]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$").unwrap();
    static ref FULL_NAME_REGEX: Regex = Regex::new(r"^[a-zA-Z]{2,}(?: [a-zA-Z]+){0,2}$").unwrap();
}

#[derive(Clone, Debug)]
pub struct PasswordParameters {
    min_len: usize,
    upper_case: bool,
    lower_case: bool,
    number: bool,
    special: bool,
}

impl PasswordParameters {
    pub fn new(min_len: usize, upper_case: bool, lower_case: bool, number: bool, special: bool) -> Self {
        Self {
            min_len,
            upper_case,
            lower_case,
            number,
            special,
        }
    }
}

impl Default for PasswordParameters {
    fn default() -> Self {
        Self {
            min_len: 10,
            upper_case: true,
            lower_case: true,
            number: true,
            special: true,
        }
    }
}

pub fn validate_username(username: &str) -> ValidationResult {
    if USERNAME_REGEX.is_match(username) {
        Ok(())
    } else {
        Err(GeneralError::UserValidationError(UserValidationError::InvalidUsername))
    }
}

pub fn validate_email(email: &str) -> ValidationResult {
    if EMAIL_REGEX.is_match(email) {
        Ok(())
    } else {
        Err(GeneralError::UserValidationError(UserValidationError::InvalidEmail))
    }
}

pub fn validate_full_name(full_name: &str) -> ValidationResult {
    if FULL_NAME_REGEX.is_match(full_name) {
        Ok(())
    } else {
        Err(GeneralError::UserValidationError(UserValidationError::InvalidFullName))
    }
}

pub fn validate_password(password: &str, parameters: &PasswordParameters) -> ValidationResult {
    if password.len() < parameters.min_len {
        return Err(GeneralError::UserValidationError(UserValidationError::InvalidPassword(
            format!(
                "Password length must be greater or equal to {}. Current length: {}",
                parameters.min_len,
                password.len()
            ),
        )));
    }

    if parameters.upper_case {
        let mut is_upper_case_present = false;
        for c in password.chars() {
            if c.is_uppercase() {
                is_upper_case_present = true;
                break;
            }
        }
        if !is_upper_case_present {
            return Err(GeneralError::UserValidationError(UserValidationError::InvalidPassword(
                "Password must contain at least one upper case character".to_owned(),
            )));
        }
    }

    if parameters.lower_case {
        let mut is_lower_case_present = false;
        for c in password.chars() {
            if c.is_lowercase() {
                is_lower_case_present = true;
                break;
            }
        }
        if !is_lower_case_present {
            return Err(GeneralError::UserValidationError(UserValidationError::InvalidPassword(
                "Password must contain at least one lower case character".to_owned(),
            )));
        }
    }

    if parameters.number {
        let mut is_number_case_present = false;
        for c in password.chars() {
            if let '0'..='9' = c {
                is_number_case_present = true;
                break;
            }
        }
        if !is_number_case_present {
            return Err(GeneralError::UserValidationError(UserValidationError::InvalidPassword(
                "Password must contain at least one number".to_owned(),
            )));
        }
    }

    if parameters.special {
        let mut is_special_case_present = false;
        for c in password.chars() {
            if "!@#$%^&*()_+=/\\-<>:;{}[]?|`~".contains(c) {
                is_special_case_present = true;
                break;
            }
        }
        if !is_special_case_present {
            return Err(GeneralError::UserValidationError(UserValidationError::InvalidPassword(
                "Password must contain at least one special character".to_owned(),
            )));
        }
    }

    Ok(())
}

#[cfg(test)]
mod tests {
    use crate::user::{validate_email, validate_password, validate_username, PasswordParameters};

    #[test]
    fn test_valid_usernames() {
        validate_username("foo.bar").unwrap();

        validate_username("foobar").unwrap();

        validate_username("foo.bar.buzz").unwrap();

        validate_username("foo_bar").unwrap();
    }

    #[test]
    fn test_invalid_usernames() {
        assert!(validate_username("foo._bar").is_err());

        assert!(validate_username("__foo.bar").is_err());

        assert!(validate_username("foobar..").is_err());

        assert!(validate_username("_foo.bar_").is_err());
    }

    #[test]
    fn test_valid_emails() {
        validate_email("example@domain.com").unwrap();

        validate_email("example.second@domain.com").unwrap();

        validate_email("example.second@subsite.domain.com").unwrap();

        validate_email("example.second.third@subsite.domain.com").unwrap();
    }

    #[test]
    fn test_invalid_emails() {
        assert!(validate_email("example@domaincom").is_err());

        assert!(validate_email("exampledomain.com").is_err());

        assert!(validate_email("+example@domain.com").is_err());

        assert!(validate_email("example@domain.com=").is_err());

        assert!(validate_email("example@domain.").is_err());
    }

    #[test]
    fn test_password_validation() {
        let parameters = PasswordParameters::default();

        assert!(validate_password("11111", &parameters).is_err());
        assert!(validate_password("121212121212", &parameters).is_err());
        assert!(validate_password("12frvfv21212", &parameters).is_err());
        assert!(validate_password("12frvFV21212", &parameters).is_err());
        assert!(validate_password("12frvfv21#$12", &parameters).is_err());

        assert!(validate_password("12frVFv21#$12", &parameters).is_ok());
    }
}
