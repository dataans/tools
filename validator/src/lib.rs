#[cfg(feature = "user")]
pub mod user;

use thiserror::Error;

#[derive(Error, Debug)]
pub enum Error {
    #[cfg(feature = "user")]
    #[error("User validation error: {0:?}")]
    UserValidationError(crate::user::UserValidationError),
}

pub type ValidationResult = Result<(), Error>;
