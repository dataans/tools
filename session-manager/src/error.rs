use thiserror::Error;

#[derive(Error, Debug)]
pub enum SessionError {
    #[error("Redis error: {0:?}")]
    RedisConnection(#[from] redis::RedisError),
    #[error("Crypto error: {0}")]
    Crypto(String),
    #[error("Session token error: {0}")]
    SessionToken(String),
    #[error("Session is not present")]
    SessionIsNotPresent,
}

#[cfg(feature = "actix-web")]
mod actix_web {
    use std::borrow::Cow;

    use actix_web::body::BoxBody;
    use actix_web::http::header::{HeaderName, HeaderValue};
    use actix_web::http::StatusCode;
    use actix_web::web::Bytes;
    use actix_web::{HttpResponse, ResponseError};
    use serde::{Deserialize, Serialize};

    use super::SessionError;

    #[derive(Serialize, Deserialize)]
    pub struct ApiError<'a, 'b> {
        pub message: Cow<'a, str>,
        pub details: Option<Cow<'b, str>>,
    }

    pub const INTERNAL: ApiError<'static, 'static> = ApiError {
        message: Cow::Borrowed("Internal error."),
        details: None,
    };
    pub const UNAUTHORIZED: ApiError<'static, 'static> = ApiError {
        message: Cow::Borrowed("Unauthorized"),
        details: Some(Cow::Borrowed("Sign-in again.")),
    };

    impl ResponseError for SessionError {
        fn status_code(&self) -> StatusCode {
            match self {
                SessionError::SessionToken(_) => StatusCode::UNAUTHORIZED,
                SessionError::SessionIsNotPresent => StatusCode::UNAUTHORIZED,
                error => {
                    error!(error = ?error, "session internal error");

                    StatusCode::INTERNAL_SERVER_ERROR
                }
            }
        }

        fn error_response(&self) -> HttpResponse<BoxBody> {
            let mut res = HttpResponse::new(self.status_code());

            let body: Bytes = match self {
                SessionError::RedisConnection(_) => serde_json::to_vec(&INTERNAL).unwrap().into(),
                SessionError::Crypto(_) => serde_json::to_vec(&INTERNAL).unwrap().into(),
                SessionError::SessionToken(_) => serde_json::to_vec(&ApiError {
                    message: "Invalid session token".into(),
                    details: Some(self.to_string().into()),
                })
                .unwrap()
                .into(),
                SessionError::SessionIsNotPresent => serde_json::to_vec(&UNAUTHORIZED).unwrap().into(),
            };

            res.head_mut().headers_mut().insert(
                HeaderName::from_static("content-type"),
                HeaderValue::from_static("application/json"),
            );

            res.set_body(BoxBody::new(body))
        }
    }
}
