use std::rc::Rc;

use actix_service::Transform;
use actix_web::dev::{Service, ServiceRequest, ServiceResponse};
use actix_web::{Error, HttpMessage};
use async_mutex::Mutex;
use futures::future::{ready, LocalBoxFuture, Ready};
use futures::FutureExt;

use crate::{Session, SessionService, AUTH_COOKIE_NAME};

pub struct SessionMiddleware<S> {
    pub(crate) service: Rc<S>,
    pub(crate) session_service: Rc<Mutex<SessionService>>,
}

impl<S, B> Service<ServiceRequest> for SessionMiddleware<S>
where
    S: Service<ServiceRequest, Response = ServiceResponse<B>, Error = Error> + 'static,
{
    type Response = ServiceResponse<B>;
    type Error = Error;
    type Future = LocalBoxFuture<'static, Result<Self::Response, Self::Error>>;

    actix_service::forward_ready!(service);

    fn call(&self, req: ServiceRequest) -> Self::Future {
        let srv = Rc::clone(&self.service);
        let session_service = self.session_service.clone();

        async move {
            let session_cookie = match req.request().cookie(AUTH_COOKIE_NAME) {
                Some(cookie) => cookie,
                None => return srv.call(req).await,
            };

            if let Ok(session) = session_service
                .lock()
                .await
                .get_session(session_cookie.value())
                .await
            {
                req.extensions_mut().insert::<Session>(session);
            }

            let res = srv.call(req).await?;

            Ok(res)
        }
        .boxed_local()
    }
}

pub struct SessionMiddlewareFactory {
    session_service: Rc<Mutex<SessionService>>,
}

impl SessionMiddlewareFactory {
    pub fn new(session_service: Rc<Mutex<SessionService>>) -> Self {
        Self { session_service }
    }
}

impl<S, B> Transform<S, ServiceRequest> for SessionMiddlewareFactory
where
    S: Service<ServiceRequest, Response = ServiceResponse<B>, Error = Error> + 'static,
{
    type Response = ServiceResponse<B>;
    type Error = Error;
    type Transform = SessionMiddleware<S>;
    type InitError = ();
    type Future = Ready<Result<Self::Transform, Self::InitError>>;

    fn new_transform(&self, service: S) -> Self::Future {
        ready(Ok(SessionMiddleware {
            service: Rc::new(service),
            session_service: self.session_service.clone(),
        }))
    }
}
