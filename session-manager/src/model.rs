use serde::{Deserialize, Serialize};
use time::OffsetDateTime;
use uuid::Uuid;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Session {
    pub id: Uuid,
    pub user_id: Uuid,
    pub expiration_date: OffsetDateTime,
}

#[cfg(feature = "actix-web")]
mod actix_web {
    use actix_web::dev::Payload;
    use actix_web::{FromRequest, HttpMessage, HttpRequest};
    use futures::future::{ready, Ready};

    use crate::{Session, SessionError};

    impl FromRequest for Session {
        type Error = SessionError;
        type Future = Ready<Result<Self, Self::Error>>;

        fn from_request(req: &HttpRequest, _payload: &mut Payload) -> Self::Future {
            let result = req
                .extensions()
                .get::<Session>()
                .cloned()
                .ok_or(SessionError::SessionIsNotPresent);

            ready(result)
        }
    }
}
