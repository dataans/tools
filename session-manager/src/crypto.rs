use std::convert::TryInto;

use chacha20poly1305::aead::generic_array::GenericArray;
use chacha20poly1305::aead::Aead;
use chacha20poly1305::{KeyInit, XChaCha20Poly1305, XNonce};
use rand::rngs::ThreadRng;
use rand::Rng;
use uuid::Uuid;

use crate::SessionError;

pub fn random_bytes<const N: usize>(rand: &mut ThreadRng) -> [u8; N] {
    (0..N).map(|_| rand.gen::<u8>()).collect::<Vec<_>>().try_into().unwrap()
}

pub fn encrypt_uuid(id: Uuid, rand: &mut ThreadRng, cipher_key: &[u8]) -> Result<String, SessionError> {
    let cipher = XChaCha20Poly1305::new(GenericArray::from_slice(cipher_key));

    let nonce = random_bytes::<24>(rand);
    let cipher_data = cipher
        .encrypt(XNonce::from_slice(&nonce), id.as_bytes().as_slice())
        .map_err(|e| SessionError::Crypto(format!("{:?}", e)))?;

    Ok(format!("{}{}", hex::encode(nonce), hex::encode(cipher_data)))
}

pub fn decrypt_uuid(data: &str, cipher_key: &[u8]) -> Result<Uuid, SessionError> {
    let cipher = XChaCha20Poly1305::new(GenericArray::from_slice(cipher_key));

    if data.len() != 112 {
        return Err(SessionError::SessionToken("Provided session token is not valid".into()));
    }

    let nonce = &data[0..48];
    let hash = &data[48..];

    let argon_hash = cipher
        .decrypt(
            XNonce::from_slice(
                &hex::decode(nonce)
                    .map_err(|_| SessionError::SessionToken("Provided session string is not valid".into()))?,
            ),
            hex::decode(hash)
                .map_err(|_| SessionError::SessionToken("Provided session string is not valid".into()))?
                .as_slice(),
        )
        .map_err(|_| SessionError::SessionToken("Provided session string is not valid".into()))?;

    Uuid::from_slice(&argon_hash).map_err(|_| SessionError::SessionToken("Provided session string is not valid".into()))
}
