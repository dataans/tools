pub(crate) mod crypto;
pub(crate) mod error;
mod middlewares;
pub mod model;

pub use crypto::{decrypt_uuid, encrypt_uuid};
pub use error::*;
pub use middlewares::*;
pub use model::*;

#[allow(unused_imports)]
#[macro_use]
extern crate tracing;

use std::fmt;

use rand::rngs::ThreadRng;
use redis::{AsyncCommands, Client};
use serde_json::{from_str, to_string};
use time::OffsetDateTime;
use tracing::instrument;
use url::Url;
use uuid::Uuid;

use crate::model::Session;

pub const AUTH_COOKIE_NAME: &str = "SessionId";

pub const REDIS_URL_ENV: &str = "REDIS_URL";
pub const CIPHER_KEY_ENV: &str = "SESSION_ENCRYPTION_KEY";

pub const CIPHER_KEY_LEN: usize = 32;
pub type SessionCipherKey = [u8; CIPHER_KEY_LEN];

pub struct SessionService {
    redis: Client,
    cipher_key: SessionCipherKey,
    rand: ThreadRng,
}

impl SessionService {
    pub fn new_from_url_and_key(redis_url: Url, cipher_key: SessionCipherKey) -> Result<Self, SessionError> {
        Ok(SessionService {
            redis: Client::open(redis_url)?,
            cipher_key,
            rand: ThreadRng::default(),
        })
    }

    pub fn new_from_env() -> Self {
        let cipher_key = std::env::var(CIPHER_KEY_ENV).unwrap();
        assert_eq!(cipher_key.len(), 64);

        SessionService {
            redis: Client::open(std::env::var(REDIS_URL_ENV).unwrap()).unwrap(),
            cipher_key: hex::decode(cipher_key).unwrap().try_into().unwrap(),
            rand: ThreadRng::default(),
        }
    }

    #[instrument(level = "debug", ret, skip(self))]
    pub async fn new_session(
        &mut self,
        user_id: Uuid,
        expiration_date: OffsetDateTime,
    ) -> Result<String, SessionError> {
        let session = Session {
            id: Uuid::new_v4(),
            user_id,
            expiration_date,
        };

        let mut connection = self.redis.get_async_connection().await?;

        connection
            .set::<String, String, ()>(session.id.to_string(), to_string(&session).unwrap())
            .await?;

        encrypt_uuid(session.id, &mut self.rand, &self.cipher_key)
    }

    #[instrument(level = "debug", ret, skip(self))]
    pub async fn get_session(&self, session_id: &str) -> Result<Session, SessionError> {
        let session_id = decrypt_uuid(session_id, &self.cipher_key)?;

        debug!(?session_id);

        let mut connection = self.redis.get_async_connection().await?;

        let session_json: String = connection.get(session_id.to_string()).await?;

        Ok(from_str(&session_json).unwrap())
    }
}

impl fmt::Debug for SessionService {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "SessionService")
    }
}

#[cfg(test)]
mod tests {
    use time::OffsetDateTime;
    use uuid::Uuid;

    use crate::SessionService;

    async fn create_session(session_service: &mut SessionService, user_id: &Uuid) -> (String, OffsetDateTime) {
        let expiration_date = OffsetDateTime::now_utc();

        (
            session_service
                .new_session(user_id.clone(), expiration_date.clone())
                .await
                .unwrap(),
            expiration_date,
        )
    }

    #[tokio::test]
    async fn test_session_creation() {
        let mut session_service =
            SessionService::new_from_url_and_key("redis://127.0.0.1/".try_into().unwrap(), [64; 32]).unwrap();

        let user_id = Uuid::new_v4();
        let expiration_date = time::OffsetDateTime::now_utc();

        let _session_id = session_service.new_session(user_id, expiration_date).await.unwrap();
    }

    #[tokio::test]
    async fn test_get_session() {
        let mut session_service =
            SessionService::new_from_url_and_key("redis://127.0.0.1/".try_into().unwrap(), [64; 32]).unwrap();

        let user_id = Uuid::new_v4();
        let (session_id, _expiration_date) = create_session(&mut session_service, &user_id).await;

        let session = session_service.get_session(&session_id).await.unwrap();

        assert_eq!(session.user_id, user_id);
    }
}
