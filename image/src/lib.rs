pub mod generate;
pub mod storage;

#[allow(unused_imports)]
#[macro_use]
extern crate tracing;

use image::{ImageBuffer, Rgb};
use sha3::Digest;
use thiserror::Error;

pub type ImgBuff = ImageBuffer<Rgb<u8>, Vec<u8>>;

#[derive(Error, Debug)]
pub enum ImageError {
    #[error("Image processing error: {0:?}")]
    ImageError(#[from] image::ImageError),
    #[cfg(feature = "gcp-cloud-storage")]
    #[error("GCP Cloud Storage Error: {0:?}")]
    CloudStorageError(#[from] cloud_storage::Error),
    #[cfg(feature = "copy-from-url")]
    #[error("Can not fetch: {0:?}")]
    Fetch(#[from] reqwest::Error),
}

pub fn transform_file_name(filename: &str) -> String {
    let (name, ext) = if let Some(index) = filename.rfind('.') {
        (
            &filename[0..index],
            if index != 0 && index != filename.len() - 1 {
                &filename[(index + 1)..]
            } else {
                ""
            },
        )
    } else {
        (filename, "")
    };

    let mut sha256 = sha3::Sha3_256::new();
    sha256.update(name.as_bytes());

    format!("{}.{}", hex::encode(sha256.finalize()), ext)
}
