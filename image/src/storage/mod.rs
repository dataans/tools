#[cfg(feature = "gcp-cloud-storage")]
pub mod cloud_storage;
#[cfg(feature = "mock-storage")]
pub mod mock_storage;

use async_trait::async_trait;

use crate::{ImageError, ImgBuff};

#[async_trait]
pub trait StorageUtils {
    async fn save_raw_image(
        &self,
        image: Vec<u8>,
        bucket: &str,
        filepath: &str,
        content_type: &str,
    ) -> Result<(), ImageError>;

    async fn save_image(&self, image: ImgBuff, bucket: &str, filepath: &str) -> Result<(), ImageError>;

    #[cfg(feature = "copy-from-url")]
    async fn copy_image_from_url(&self, image_url: url::Url, bucket: &str) -> Result<String, ImageError>;
}

pub trait StorageUtilsFactory {
    fn build_storage_utils(&self) -> Box<dyn StorageUtils>;
    fn boxed_clone(&self) -> Box<dyn StorageUtilsFactory + Send>;
}
