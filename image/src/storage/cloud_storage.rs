use async_trait::async_trait;
use cloud_storage::client::Client;
use image::codecs::png::PngEncoder;
use image::{ColorType, EncodableLayout, ImageEncoder};
#[cfg(feature = "copy-from-url")]
use url::Url;

use super::{StorageUtils, StorageUtilsFactory};
use crate::{ImageError, ImgBuff};

pub struct CloudStorageUtils {
    client: Client,
}

impl CloudStorageUtils {
    pub fn new() -> Self {
        Self {
            client: Default::default(),
        }
    }
}

#[async_trait]
impl StorageUtils for CloudStorageUtils {
    #[instrument(level = "debug", ret, skip(self))]
    async fn save_raw_image(
        &self,
        image: Vec<u8>,
        bucket: &str,
        filepath: &str,
        content_type: &str,
    ) -> Result<(), ImageError> {
        self.client
            .object()
            .create(bucket, image, filepath, content_type)
            .await?;

        Ok(())
    }

    #[instrument(level = "debug", ret, skip(self))]
    async fn save_image(&self, image: ImgBuff, bucket: &str, filepath: &str) -> Result<(), ImageError> {
        let mut buff = Vec::new();

        PngEncoder::new(&mut buff).write_image(image.as_bytes(), image.width(), image.height(), ColorType::Rgb8)?;

        self.client.object().create(bucket, buff, filepath, "image/png").await?;

        Ok(())
    }

    #[cfg(feature = "copy-from-url")]
    #[instrument(ret, skip(self))]
    async fn copy_image_from_url(&self, image_url: Url, bucket: &str) -> Result<String, ImageError> {
        use sha3::Digest;

        let mut image_url_hash = sha3::Sha3_256::new();
        image_url_hash.update(image_url.as_str().as_bytes());

        let client = reqwest::Client::new();

        let response = client.get(image_url).send().await?;

        let image_mime_type = response
            .headers()
            .get("Content-Type")
            .map(|v| v.to_str().unwrap_or("image/png").to_owned())
            .unwrap_or_else(|| "image/png".into());

        let image_format = image_mime_type.split('/').nth(1).unwrap_or("png");

        let filename = format!("{}.{}", hex::encode(image_url_hash.finalize()), image_format);
        let image_data = response.bytes().await?.to_vec();

        self.client
            .object()
            .create(bucket, image_data, &filename, &image_mime_type)
            .await?;

        Ok(format!("https://{}/{}", bucket, filename))
    }
}

impl Default for CloudStorageUtils {
    fn default() -> Self {
        Self::new()
    }
}

#[derive(Clone, Default)]
pub struct CloudStorageFactory;

impl StorageUtilsFactory for CloudStorageFactory {
    fn build_storage_utils(&self) -> Box<dyn StorageUtils> {
        Box::new(CloudStorageUtils::default())
    }

    fn boxed_clone(&self) -> Box<dyn StorageUtilsFactory + Send> {
        Box::new(self.clone())
    }
}

#[cfg(test)]
mod tests {
    use crate::generate::avatar;
    use crate::storage::cloud_storage::CloudStorageUtils;

    #[tokio::test]
    async fn test_image_saving() {
        let avatar = avatar();
        let cloud_storage = CloudStorageUtils::new();

        cloud_storage
            .save_image(avatar, "avatars.dataans.com", "test_avatar_2.png")
            .await
            .unwrap();
    }

    #[tokio::test]
    async fn test_copy_from_url() {
        let cloud_storage = CloudStorageUtils::new();

        let avatar_url = cloud_storage
            .copy_image_from_url(
                "https://avatars.githubusercontent.com/u/43034350?v=4",
                "avatars.dataans.com",
            )
            .await
            .unwrap();
        println!("{}", avatar_url);
    }
}
