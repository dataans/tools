use std::borrow::Cow;

use async_trait::async_trait;

use super::{StorageUtils, StorageUtilsFactory};
use crate::{ImageError, ImgBuff};

#[derive(Debug, Default)]
pub struct MockStorage<'a> {
    #[allow(dead_code)]
    host: Cow<'a, str>,
}

impl<'a> MockStorage<'a> {
    pub fn new(host: impl Into<Cow<'a, str>>) -> Self {
        Self { host: host.into() }
    }
}

#[async_trait]
impl<'a> StorageUtils for MockStorage<'a> {
    #[instrument(level = "debug", ret, skip(self))]
    async fn save_raw_image(
        &self,
        _image: Vec<u8>,
        bucket: &str,
        filepath: &str,
        content_type: &str,
    ) -> Result<(), ImageError> {
        Ok(())
    }

    #[instrument(level = "debug", ret, skip(self, image))]
    async fn save_image(&self, image: ImgBuff, bucket: &str, filepath: &str) -> Result<(), ImageError> {
        trace!(?image);
        Ok(())
    }

    #[cfg(feature = "copy-from-url")]
    #[instrument(level = "debug", ret, skip(self))]
    async fn copy_image_from_url(&self, _image_url: url::Url, bucket: &str) -> Result<String, ImageError> {
        Ok(format!("https://{}/{}/mock_image.png", self.host.as_ref(), bucket))
    }
}

#[derive(Clone)]
pub struct MockStorageFactory {
    host: String,
}

impl MockStorageFactory {
    pub fn new(host: impl Into<String>) -> Self {
        Self { host: host.into() }
    }
}

impl StorageUtilsFactory for MockStorageFactory {
    fn build_storage_utils(&self) -> Box<dyn StorageUtils> {
        Box::new(MockStorage::new(self.host.clone()))
    }

    fn boxed_clone(&self) -> Box<dyn StorageUtilsFactory + Send> {
        Box::new(self.clone())
    }
}
