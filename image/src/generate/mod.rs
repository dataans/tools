pub(crate) mod patterns;

use image::{ImageBuffer, Rgb};
use rand::Rng;

use crate::generate::patterns::get_pattern;
use crate::ImgBuff;

pub const DEFAULT_COLOR: Rgb<u8> = Rgb([120, 92, 55]);

pub fn avatar() -> ImgBuff {
    let mut rand = rand::thread_rng();
    let pattern = get_pattern(rand.gen::<u8>(), &mut rand);

    ImageBuffer::from_fn(800, 800, move |x, y| pattern.color(x as i32, y as i32))
}

#[cfg(test)]
mod tests {
    use crate::generate::avatar;

    #[test]
    fn avatar_generation_example() {
        avatar().save("avatar_example.png").unwrap();
    }

    #[test]
    fn generate_avatars() {
        for i in 0..20 {
            avatar()
                .save(format!("./avatar_examples/avatar_example_{}.png", i))
                .unwrap();
        }
    }
}
