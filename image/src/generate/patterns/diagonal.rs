use image::Rgb;
use rand::rngs::ThreadRng;
use rand::Rng;

use super::super::DEFAULT_COLOR;
use super::{Area, Pattern};

pub struct SimpleDiagonalPattern {
    areas: Vec<Area>,
}

impl SimpleDiagonalPattern {
    pub fn new(rand: &mut ThreadRng) -> Self {
        let mut rand_color = || Rgb([rand.gen::<u8>(), rand.gen::<u8>(), rand.gen::<u8>()]);

        Self {
            areas: vec![
                Area::new(Box::new(|x, y| y > x + 200), rand_color()),
                Area::new(Box::new(|x, y| y < x - 150), rand_color()),
                Area::new(Box::new(|x, y| (y < x + 200) && (y > x - 150)), rand_color()),
            ],
        }
    }
}

impl Pattern for SimpleDiagonalPattern {
    fn color(&self, x: i32, y: i32) -> Rgb<u8> {
        for area in &self.areas {
            if let Some(color) = area.colorize(x, y) {
                return color;
            }
        }
        DEFAULT_COLOR
    }
}

pub struct MultiDiagonalPattern {
    areas: Vec<Area>,
}

impl MultiDiagonalPattern {
    pub fn new(rand: &mut ThreadRng) -> Self {
        let fdk = rand.gen::<i32>().abs() % 400;
        let fdw = rand.gen::<i32>().abs() % 200 + 50;
        let sdk = rand.gen::<i32>().abs() % 300 + 400;
        let sdw = rand.gen::<i32>().abs() % 200 + 50;

        let f_top = fdk + fdw;
        let f_bottom = fdk;
        let s_top = sdk + sdw;
        let s_bottom = sdk;

        let mut rand_color = || Rgb([rand.gen::<u8>(), rand.gen::<u8>(), rand.gen::<u8>()]);

        Self {
            areas: vec![
                Area::new(Box::new(move |x, y| (y > x + f_top) && (y > -x + s_top)), rand_color()),
                Area::new(
                    Box::new(move |x, y| (y > -x + s_bottom) && (y < -x + s_top) && (y > x + f_top)),
                    rand_color(),
                ),
                Area::new(
                    Box::new(move |x, y| (y < -x + s_bottom) && (y > x + f_top)),
                    rand_color(),
                ),
                Area::new(
                    Box::new(move |x, y| (y < x + f_top) && (y > -x + s_top) && (y > x + f_bottom)),
                    rand_color(),
                ),
                Area::new(
                    Box::new(move |x, y| {
                        (y < x + f_top) && (y < -x + s_top) && (y > -x + s_bottom) && (y > x + f_bottom)
                    }),
                    rand_color(),
                ),
                Area::new(
                    Box::new(move |x, y| (y < x + f_top) && (y < -x + s_bottom) && (y > x + f_bottom)),
                    rand_color(),
                ),
                Area::new(
                    Box::new(move |x, y| (y < x + f_bottom) && (y > -x + s_top)),
                    rand_color(),
                ),
                Area::new(
                    Box::new(move |x, y| (y < -x + s_top) && (y < x + f_bottom) && (y > -x + s_bottom)),
                    rand_color(),
                ),
                Area::new(
                    Box::new(move |x, y| (y < x + f_bottom) && (y < -x + s_bottom)),
                    rand_color(),
                ),
            ],
        }
    }
}

impl Pattern for MultiDiagonalPattern {
    fn color(&self, x: i32, y: i32) -> Rgb<u8> {
        for area in &self.areas {
            if let Some(color) = area.colorize(x, y) {
                return color;
            }
        }
        DEFAULT_COLOR
    }
}
