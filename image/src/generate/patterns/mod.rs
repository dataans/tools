pub mod circle;
pub mod diagonal;

use image::Rgb;
use rand::rngs::ThreadRng;

use crate::generate::patterns::circle::{MultiCirclePattern, SimpleSirclePattern};
use crate::generate::patterns::diagonal::{MultiDiagonalPattern, SimpleDiagonalPattern};

pub trait Pattern {
    fn color(&self, x: i32, y: i32) -> Rgb<u8>;
}

pub struct Area {
    predicate: Box<dyn Fn(i32, i32) -> bool>,
    color: Rgb<u8>,
}

pub fn get_pattern(n: u8, rand: &mut ThreadRng) -> Box<dyn Pattern> {
    if n < 30 {
        Box::new(SimpleDiagonalPattern::new(rand))
    } else if n < 60 {
        Box::new(SimpleSirclePattern::new(rand))
    } else if n < 170 {
        Box::new(MultiDiagonalPattern::new(rand))
    } else {
        Box::new(MultiCirclePattern::new(rand))
    }
}

impl Area {
    pub fn new(predicate: Box<dyn Fn(i32, i32) -> bool>, color: Rgb<u8>) -> Self {
        Self { predicate, color }
    }

    pub fn colorize(&self, x: i32, y: i32) -> Option<Rgb<u8>> {
        if (self.predicate)(x, y) {
            Some(self.color)
        } else {
            None
        }
    }
}
