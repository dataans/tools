use image::Rgb;
use rand::rngs::ThreadRng;
use rand::Rng;

use super::super::DEFAULT_COLOR;
use super::{Area, Pattern};

#[derive(Copy, Clone)]
struct Circle {
    pub x: i32,
    pub y: i32,
    pub radius: i32,
}

impl Circle {
    pub fn inside(&self, x: i32, y: i32) -> bool {
        (x + self.x).pow(2) + (y + self.y).pow(2) <= self.radius
    }

    pub fn outside(&self, x: i32, y: i32) -> bool {
        !self.inside(x, y)
    }
}

pub struct SimpleSirclePattern {
    areas: Vec<Area>,
}

impl SimpleSirclePattern {
    pub fn new(rand: &mut ThreadRng) -> Self {
        let mut rand_color = || Rgb([rand.gen::<u8>(), rand.gen::<u8>(), rand.gen::<u8>()]);

        Self {
            areas: vec![
                Area::new(Box::new(|x, y| (x - 850).pow(2) + y.pow(2) > 700000), rand_color()),
                Area::new(Box::new(|x, y| (x - 850).pow(2) + y.pow(2) < 500000), rand_color()),
                Area::new(
                    Box::new(|x, y| ((x - 850).pow(2) + y.pow(2) < 700000) && ((x - 850).pow(2) + y.pow(2) > 500000)),
                    rand_color(),
                ),
            ],
        }
    }
}

impl Pattern for SimpleSirclePattern {
    fn color(&self, x: i32, y: i32) -> Rgb<u8> {
        for area in &self.areas {
            if let Some(color) = area.colorize(x, y) {
                return color;
            }
        }
        DEFAULT_COLOR
    }
}

pub struct MultiCirclePattern {
    areas: Vec<Area>,
}

impl MultiCirclePattern {
    pub fn new(rand: &mut ThreadRng) -> Self {
        let f_x = -(rand.gen::<i32>().abs() % 400 + 400);
        let f_y = -rand.gen::<i32>().abs() % 300;
        let f_radius = rand.gen::<i32>().abs() % 200_000 + 300_000;
        let f_width = rand.gen::<i32>().abs() % 200_000 + 100_000;

        let s_x = -(rand.gen::<i32>().abs() % 400 + 600);
        let s_y = -(rand.gen::<i32>().abs() % 500 + 400);
        let s_radius = rand.gen::<i32>().abs() % 200_000 + 200_000;
        let s_width = rand.gen::<i32>().abs() % 200_000 + 100_000;

        let f_in = Circle {
            x: f_x,
            y: f_y,
            radius: f_radius,
        };
        let f_out = Circle {
            x: f_x,
            y: f_y,
            radius: f_radius + f_width,
        };

        let s_in = Circle {
            x: s_x,
            y: s_y,
            radius: s_radius,
        };
        let s_out = Circle {
            x: s_x,
            y: s_y,
            radius: s_radius + s_width,
        };

        let mut rand_color = || Rgb([rand.gen::<u8>(), rand.gen::<u8>(), rand.gen::<u8>()]);

        Self {
            areas: vec![
                Area::new(
                    Box::new(move |x, y| f_out.outside(x, y) && s_in.inside(x, y)),
                    rand_color(),
                ),
                Area::new(
                    Box::new(move |x, y| s_out.inside(x, y) && s_in.outside(x, y) && f_out.outside(x, y)),
                    rand_color(),
                ),
                Area::new(
                    Box::new(move |x, y| s_out.outside(x, y) && f_out.outside(x, y)),
                    rand_color(),
                ),
                Area::new(
                    Box::new(move |x, y| f_out.inside(x, y) && f_in.outside(x, y) && s_in.inside(x, y)),
                    rand_color(),
                ),
                Area::new(
                    Box::new(move |x, y| {
                        f_out.inside(x, y) && f_in.outside(x, y) && s_in.outside(x, y) && s_out.inside(x, y)
                    }),
                    rand_color(),
                ),
                Area::new(
                    Box::new(move |x, y| f_out.inside(x, y) && s_out.outside(x, y) && f_in.outside(x, y)),
                    rand_color(),
                ),
                Area::new(
                    Box::new(move |x, y| f_in.inside(x, y) && s_in.inside(x, y)),
                    rand_color(),
                ),
                Area::new(
                    Box::new(move |x, y| s_in.outside(x, y) && f_in.inside(x, y) && s_out.inside(x, y)),
                    rand_color(),
                ),
                Area::new(
                    Box::new(move |x, y| f_in.inside(x, y) && s_out.outside(x, y)),
                    rand_color(),
                ),
            ],
        }
    }
}

impl Pattern for MultiCirclePattern {
    fn color(&self, x: i32, y: i32) -> Rgb<u8> {
        for area in &self.areas {
            if let Some(color) = area.colorize(x, y) {
                return color;
            }
        }
        DEFAULT_COLOR
    }
}
